public class ContactAndLeadSearch {
    
    
    public static List<List< SObject>> searchContactsAndLeads(String temp){
        
        
        List<List<sObject>> searchList = [FIND :temp IN ALL FIELDS 
                   							RETURNING Lead,
                                            Contact];
        return searchList;
    }

}