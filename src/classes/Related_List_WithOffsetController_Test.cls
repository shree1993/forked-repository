/***********************************************************************
 Class          : Related_ListController_Test
 Author         : Appirio
 Descritption   : Provide test coverage to Related_ListController.cls
 ************************************************************************/
@isTest
private class Related_List_WithOffsetController_Test {
    
    static Account account;
    // Method to test search functionality
    static testMethod void testSearchListFunctionality() {
        
        createTestData();
        
        Test.startTest();
        
            Related_List_WithOffsetController controller = new Related_List_WithOffsetController();
            // Setting attributes for searching
            controller.fieldsCSV = 'FirstName,LastName,Account.Owner.FirstName';
            controller.sortDirection = 'asc';
            controller.objectName = 'Contact';
            controller.pageSize = 5;
            controller.searchFieldName = 'AccountId';
            controller.searchFieldValue = account.id;
            controller.orderByFieldName = 'LastName';
            controller.sortByField  = 'LastName';
            controller.filter = '';
            controller.title = 'Contacts';
            controller.returnUrl = '/003/0';
            controller.moreLink = 'true';
            controller.showmoreLink = 'true';
            controller.showAsStandardRelatedList = true;

            // Calling search method
            //controller.getRecords();
            List<sObject> records = controller.getRecords();
            system.assertEquals(records.size(), 5, 'Search method should return 5 records.');
            
            controller.Beginning();
            controller.Previous();
            controller.Next();
            controller.End();
            controller.getDisablePrevious();
            controller.getDisableNext();
            controller.getTotal_size();
            controller.getPageNumber();
            controller.getTotalPages();
            controller.showMore();
            controller.getShowNewButton();
            controller.sortByFieldAction();
            
            controller.deleteRecordId = controller.getRecords().get(0).id;
            controller.deleteRecord();
            system.assertEquals(controller.getRecords().size(), 4, 'After delete, size of the record list should be 4.');
            
        Test.stopTest();
    }
    
    // Method to test validations
    static testMethod void testValidation() {
        createTestData();
        Test.startTest();
            Related_List_WithOffsetController controller = new Related_List_WithOffsetController();
            // Setting attributes for searching
            controller.sortDirection = 'asc';
            controller.objectName = 'Contact';
            controller.pageSize = 5;
            controller.searchFieldName = 'AccountId';
            controller.searchFieldValue = account.id;
            controller.orderByFieldName = 'LastName';
            controller.sortByField  = 'LastName';
            controller.filter = '';
            controller.title = 'Contacts';
            controller.returnUrl = '/003/0';
            controller.moreLink = 'true';
            controller.showmoreLink = 'true';
            controller.showAsStandardRelatedList = true;
            
            // Calling search method without field list
            List<sObject> records = controller.getRecords();
            System.assert(ApexPages.getMessages().get(0).getSummary().contains('fieldList or fieldsCSV attribute must be defined.') );
            
            // Setting field list
            List<String> fieldList = new List<String>();
            fieldList.add('FirstName');
            fieldList.add('LastName');
            controller.fieldsList = fieldList;
            
            // Calling search method with incorrect sortDirection
            controller.sortDirection = 'xyz';
            records = controller.getRecords();
            System.assert(ApexPages.getMessages().get(1).getSummary().contains('sortDirection attribute must have value of "asc" or "desc"') );
            
            controller.sortDirection = 'asc';
            records = controller.getRecords();
        
        Test.stopTest();
        
    }
    
    
    // Method to create test data
    private static void createTestData(){
        
        account = new Account(Name = 'test account');
        insert account;
        
        List<Contact> listContact = new List<Contact>();
        for(Integer indx=1; indx <= 5; indx ++){
            Contact contact = new Contact(LastName = 'Test' + String.valueOf(indx), AccountId = account.id);
            listContact.add(contact);
        }
        insert listContact;
    }
}